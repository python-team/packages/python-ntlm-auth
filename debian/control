Source: python-ntlm-auth
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Harlan Lieberman-Berg <hlieberman@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
	       dh-python,
	       python3,
	       python3-setuptools,
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-ntlm-auth
Vcs-Git: https://salsa.debian.org/python-team/packages/python-ntlm-auth.git
Homepage: https://github.com/jborean93/ntlm-auth

Package: python3-ntlm-auth
Architecture: all
Depends: ${python3:Depends},
	 ${misc:Depends}
Recommends: python3-cryptography
Description: NTLM low-level Python library
 This library handles the low-level details of NTLM authentication for
 use in authenticating with a service that uses NTLM. It will create
 and parse the 3 different message types in the order required and
 produce a base64 encoded value that can be attached to the HTTP
 header.
 .
 The goal of this library is to offer full NTLM support including
 signing and sealing of messages as well as supporting MIC for message
 integrity and the ability to customise and set limits on the messages
 sent.
